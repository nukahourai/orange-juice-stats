# 100% Orange Juice Player Stats

This is the public repository of the 100% Orange Juice Player Stats website.

## Libraries

This project uses the following libraries which are included in the repository.

* Bootstrap
* Font Awesome
* DataTables
* Benjamin Smith's SteamAuthentication
* Mukunda Johnson's SteamID Parser
* Jumpy's Alt string DataTable Plugin

## Prerequisites

This project was developed under PHP 7.0 and Apache 2.4. Other PHP versions and other webservers were not tested.

You need a proper SteamAPI Key to make this work. You can get one on the [Steam's Web API website](https://steamcommunity.com/dev). The API keys goes in `steamauth/SteamConfig.php`.

## Contributing

Please contribute to the project! A merge request and stat documentation is always welcome.

## License

This project is licensed under the Affero GPLv3 License - see the [LICENSE](https://gitlab.com/nukahourai/orange-juice-stats/blob/master/LICENSE) file for details.

In a nutshell, it's the same license as the regular GPLv3 but if you host this project elsewhere you MUST distribute the source-code. 