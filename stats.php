<?php
$host = rtrim(str_replace('/utils/', '', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://").$_SERVER['HTTP_HOST'].str_replace(pathinfo(__FILE__, PATHINFO_FILENAME).".php", "", $_SERVER['SCRIPT_NAME'])), '/');
if(isset($_POST['buffer']))
{
    require ('steamauth/steamauth.php');
    require ('steamauth/SteamConfig.php');
    require ('utils/steamid.php');
    SteamID::SetParseRawDefault(true);
    SteamID::SetSteamAPIKey($steamauth['apikey']);

    $buff = filter_var($_POST['buffer'], FILTER_SANITIZE_STRING);

    $trying = 1;
    while($trying < 7)
    {
        $steamid = SteamID::Parse($buff, $trying);

        if(!$steamid)
            $trying++;
        else
            break;
    }

    if(!$steamid)
        header('Location: '.$host.'/stats.php?error');

    $steamid = $steamid->Format( SteamID::FORMAT_STEAMID64 );

    header('Location: '.$host.'/stats.php?uid='.$steamid);
}
?>
<!DOCTYPE html>

<?php
    require ('steamauth/steamauth.php');
    require ('steamauth/SteamConfig.php');
    unset($_SESSION['steam_uptodate']);
?>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico?' />
        <link rel="stylesheet" href="css/bootstrap.min.css" />.
        <link rel="stylesheet" href="css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/brands.css" integrity="sha384-rf1bqOAj3+pw6NqYrtaE1/4Se2NBwkIfeYbsFdtiR6TQz0acWiwJbv1IM/Nt/ite" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css" integrity="sha384-1rquJLNOM3ijoueaaeS5m+McXPJCGdr5HcA03/VHXxcp2kX2sUrQDmFc3jR5i/C7" crossorigin="anonymous">        <link rel="stylesheet" href="css/common.css" />
        <title>100%OJ Player Stats</title>
    </head>
    <body>
        <div id="wrap">
            <div id="main" class="clear-top">
                <div class="header sticky" id="pHeader">
                    <nav>
                        <a onclick="toggle_menu()" id="menu-icon"></a>
                        <div id="mobile_menu" style="display:none;">
<?php
    if(!isset($_SESSION['steamid']))
    {
?>
                            <div align="center">
                                <?php loginbutton("rectangle"); ?>
                                <br>
                                <br>
                            </div>
<?php
    }
    else
    {
        include ('steamauth/userInfo.php');
?>
                            <table>
                                <tr>
                                    <th style="width:32px">
                                        <img class="avatar" src="<?php echo $_SESSION['steam_avatar']; ?>" />
                                    </th>
                                    <th style="padding-left: 10px;padding-right: 10px;">
                                        <h4 style="vertical-align:middle;display:table-cell;"><?php echo $steamprofile['personaname']; ?></h4>
                                    </th>
                                </tr>
                            </table>
                            <div align="center">
                                <br>
                                <?php logoutbutton(); ?>
                                <br>
                            </div>
<?php
    }
?>
                            <form action="stats.php" method="POST">
                                <input class="form-control" placeholder="SteamID or Custom URL" style="display:inline;width:100%" type="text" name="buffer" />
                            </form>
                        </div>
                    </nav>
                    <div class="desktop_header hide_on_mobile">
<?php
    if(!isset($_SESSION['steamid']))
    {
?>
                        <div style="float:left">
                            <?php loginbutton("rectangle"); ?>
                        </div>
<?php
    }
    else
    {
        include ('steamauth/userInfo.php');
?>
                        <div style="float:left">
                            <table>
                                <tr>
                                    <th>
                                        <img class="avatar" src="<?php echo $_SESSION['steam_avatar']; ?>" />
                                    </th>
                                    <th style="padding-left: 10px;padding-right: 10px;">
                                        <h4 style="vertical-align:middle;display:table-cell;"><?php echo $steamprofile['personaname']; ?></h4>
                                    </th>
                                    <th>
                                        <?php logoutbutton(); ?>
                                    </th>
                                </tr>
                            </table>
                        </div>
<?php
    }
?>
                        <div style="float:right !important">
                            <div class="input-append">
                                <form action="stats.php" method="POST">
                                    <input class="form-control" placeholder="SteamID32, SteamID64, or Custom URL" style="display:inline;width:320px" type="text" name="buffer" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:50px">
                    <div class="col-xs-12 col-lg-12" align="center">
                        <a style="display:inline-block;" href="<?php echo $host ?>"><img class="banner" alt="Knowing our community..." src="imgs/header.png"/></a>
                    </div>
                </div>
<?php
    $url_orangestats = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=282800&key=".$steamauth['apikey']."&steamid=%SID%";
    $url_publicprofile = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=".$steamauth['apikey']."&steamids=%SID%";
    if(isset($_GET['error']))
    {
?>
                <div class="row">
                    <div class="col-xs-12 col-lg-12" align="center">
                        <div class="box">
                            <div id="stat_container">
                                <p>An error has occured when trying to find that SteamID, try with a different format.</p>
                            </div>
                        </div>
                    </div>
                </div>
<?php
    }
    elseif(isset($_GET['uid']) || isset($_SESSION['steamid']))
    {
        $url_publicprofile = str_replace("%SID%", (isset($_GET['uid'])) ? filter_var($_GET['uid'], FILTER_SANITIZE_STRING) : $_SESSION['steamid'], $url_publicprofile);
        $profile = file_get_contents($url_publicprofile);
        if ($profile === false)
        {
?>
                <div class="row">
                    <div class="col-xs-12 col-lg-12" align="center">
                        <div class="box">
                            <div id="stat_container">
                                <p>An error has occured when trying to find that SteamID, try with a different format.</p>
                            </div>
                        </div>
                    </div>
                </div>
<?php
        }
        else
        {
            echo "<code style='display:none'>".print_r($profile, true)."</code>";
            $profile = json_decode($profile, true)['response']['players'][0];
?>
                <div class="row">
                    <div class="col-xs-12 col-lg-12" align="center">
                        <div class="box">
                            <div id="stat_container">
                                <table id="table_summary_desktop" class="table-sm hide_on_mobile" style="width:100%">
                                    <tr>
                                        <th rowspan="5">
                                            <a href="<?php echo $profile["profileurl"];?>">
                                                <img class="avatar_big hide_on_mobile" src="<?php echo $profile['avatarfull'] ?>"/>
                                            </a>
                                        </th>
                                        <th colspan="4">
                                            <h2><?php echo $profile['personaname'] ?>'s Stats</h2>
                                        </th>
                                    </tr>
<?php
            $url_orangestats = str_replace("%SID%", (isset($_GET['uid'])) ? filter_var($_GET['uid'], FILTER_SANITIZE_STRING) : $_SESSION['steamid'], $url_orangestats);
            $stats = file_get_contents($url_orangestats);
            if ($stats === false)
            {
?>
                                    <tr>
                                        <td colspan="4" rowspan="4">
                                            <p>I can't access this users' stats. Verify their game stats are set to public.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
<?php
            }
            else
            {
                $stats = json_decode($stats, true)['playerstats'];
?>
                                    <tr>
                                        <td>
                                            <p class="statname">Online Games Played:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][5]['value']; ?></p>
                                        </td>
                                        <td>
                                            <p class="statname">Player EXP:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][4]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Online Games Won:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][6]['value']; ?></p>
                                        </td>
                                        <td>
                                            <p class="statname">NPC Defeats:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][7]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname"><abbr title="This value is calculated using the &quot;Online Games&quot; and &quot;Online Wins&quot; stats. It's not a tracked value per se. This value is rounded up.">Win Rate:</abbr></p>
                                        </td>
                                        <td>
                                            <p><?php echo ceil($stats['stats'][6]['value']/$stats['stats'][5]['value']*100); ?>%</p>
                                        </td>
                                        <td>
                                            <!-- <p class="statname">Pacifist Wins:</p> -->
                                        </td>
                                        <td>
                                            <!-- <p><?php echo $stats['stats'][54]['value']; ?></p> -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Player Level:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][3]['value']; ?></p>
                                        </td>
                                        <td>
                                            <!-- <p class="statname">Duelist Wins:</p> -->
                                        </td>
                                        <td>
                                            <!-- <p><?php echo $stats['stats'][55]['value']; ?></p> -->
                                        </td>
                                    </tr>
                                </table>
<?php
            }
?>
                                <table id="table_summary_mobile" class="table-sm" style="width:90%">
                                    <tr>
                                        <th colspan="2">
                                            <h2 style="text-align:center"><?php echo $profile['personaname'] ?>'s Stats</h2>
                                        </th>
                                    </tr>
<?php
            $url_orangestats = str_replace("%SID%", (isset($_GET['uid'])) ? filter_var($_GET['uid'], FILTER_SANITIZE_STRING) : $_SESSION['steamid'], $url_orangestats);
            $stats = file_get_contents($url_orangestats);
            if ($stats === false)
            {
?>
                                    <tr>
                                        <td colspan="2">
                                            <p>I can't access this users' stats. Verify their game stats are set to public.</p>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
<?php
            }
            else
            {
                $stats = json_decode($stats, true)['playerstats'];
?>
                                    <tr>
                                        <td>
                                            <p class="statname">Online Games Played:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][5]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Player EXP:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][4]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Online Games Won:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][6]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">NPC Defeats:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][7]['value']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Win Rate:</p>
                                        </td>
                                        <td>
                                            <p><?php echo ceil($stats['stats'][6]['value']/$stats['stats'][5]['value']*100); ?>%</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="statname">Player Level:</p>
                                        </td>
                                        <td>
                                            <p><?php echo $stats['stats'][3]['value']; ?></p>
                                        </td>
                                    </tr>
                                </table>

                                <ul id="tab_group" class="nav nav-tabs hide_on_mobile">
                                    <li class="nav-item">
                                        <a class="nav-link active" onclick="changeTab('tab_chara_stats', this)">Character Stats</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" onclick="changeTab('tab_card_stats', this)">Card Stats</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" onclick="changeTab('tab_event_stats', this)">Event Stats</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" onclick="changeTab('tab_misc_stats', this)">Misc Stats</a>
                                    </li>
                                </ul>

                                <!-- I'm lazy -->
                                <div id="select_tab">
                                    <select onchange="changeTab(this, null)" class="form-control">
                                        <option value=0>Character Stats</option>
                                        <option value=1>Card Stats</option>
                                        <option value=2>Event Stats</option>
                                        <option value=3>Misc Stats</option>
                                    </select>
                                    <br>
                                </div>

                                <div id="tab_chara_stats" class="tabcontent">
                                    <table id="dt_chara_stats" class="table table-striped table-bordered" style="table-layout: fixed;word-wrap:break-word;border-top:none !important;width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="tabtable_header"></th>
                                                <th class="tabtable_header">Games</th>
                                                <th class="tabtable_header">Wins</th>
                                                <th class="tabtable_header">Win Rate</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Games</th>
                                                <th>Wins</th>
                                                <th>Win Rate</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>
                                </div>

                                <div id="tab_card_stats" style="display:none" class="tabcontent">
                                    <table id="dt_card_stats" class="table table-striped table-bordered" style="table-layout: fixed;word-wrap:break-word;border-top:none !important;width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="tabtable_header"></th>
                                                <th class="tabtable_header">Times Chosen</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Times Chosen</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>
                                    <p>Mixed Booster Pack, Acceleration Pack and Community Pack stats are not tracked by the game.</p>
                                    <br>
                                </div>

                                <div id="tab_event_stats" style="display:none" class="tabcontent">
                                    <table id="dt_event_stats" class="table table-striped table-bordered" style="table-layout: fixed;word-wrap:break-word;border-top:none !important;width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="tabtable_header"></th>
                                                <th class="tabtable_header">Score</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Score</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>
                                    <p>Other events coming soon.</p>
                                    <br>
                                </div>

                                <div id="tab_misc_stats" style="display:none" class="tabcontent">
                                    <table id="dt_misc_stats" class="table table-striped table-bordered" style="table-layout: fixed;word-wrap:break-word;border-top:none !important;width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="tabtable_header">Stat</th>
                                                <th class="tabtable_header">Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Stat</th>
                                                <th>Value</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>
                                    <p>Misc Stats are a variety of stats that are too ambigous, either because of their name or position in the API, to know what they represent. Proper documentation is needed to move them to their corresponding categories.</p>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
            }
        }
    }
    else
    {
?>
                <div class="row">
                    <div class="col-xs-12 col-lg-12" align="center">
                        <div class="box">
                            <div id="stat_container">
                                <p>To check 100% Orange Juice stats either log in.</p>
                                <?php loginbutton(); ?>
                                <br><br>
                                <p>Or submit a valid SteamID in the header.</p>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
<?php
    }
?>
                <div class="hide_on_mobile">
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <footer>
            <div>
                <div class="footerbox_siteinfo">
                    <p>
                        <i>100%OJ Player Stats</i> by Gabe Iggy, <?php echo date("Y"); ?>.
                        <br>
                        This site is not affiliated with <i>orange_juice</i>, <i>Fruitbat Factory</i> or <i>Valve Corporation</i>.
                        <br>
                        This site doesn't interact with databases and as such, it doesn't store personal information.
                    </p>
                </div>
                <div class="footerbox_social">
                    <a href="https://gitlab.com/nukahourai/orange-juice-stats" class="fab fa-gitlab"></a>
                </div>
            </div>
            <br>
        </footer>
    </body>
</html>

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/datatable-plugins/alt-string.js"></script>

<script>
    var datatable_definitions = {
                "responsive": false,
                "paging": false,
                "searching": false,
                "info": false,
                "columnDefs": [
                    { "width": "5%", "type": 'alt-string', "targets": 0 },
                ],
                "order": [[ 1, "desc" ]]
            };

    var loaded_data = <?php if(isset($_GET['uid'])) echo '2'; elseif(isset($_SESSION['steamid'])) echo '1'; else echo '0'; ?>;
    var card_datatable, chara_datatable, event_datatable;
    var degrees = 0;

    $(document).ready(init());

    function persistentHeader()
    {
        if (window.pageYOffset > sticky)
            header.classList.add("sticky");
        else
            header.classList.remove("sticky");
    }

    function init()
    {

        if(loaded_data != 0)
        {
            chara_names = {
                "ALTE": "Alte",
                "ARU": "Aru",
                "ARUS": "Aru (Scramble)",
                "BOSSROBOT": "Shifu Robot",
                "CASTLE": "Flying Castle",
                "CHICKEN": "Chicken / Haruo",
                "DLC9A": "Nath",
                "DLC9B": "Tomato & Mimyuu",
                "DLC11A": "Kiriko",
                "DLC11B": "NoName",
                "DLC13A": "Ceoreparque",
                "DLC13B": "Miusaki",
                "DLC15A": "Yuki (Dangerous)",
                "DLC15B": "Tomomo (Casual) → Tomomo (Sweet Eater)",
                "DLC16A": "Tsih",
                "DLC16B": "Tequila",
                "FERNET": "Fernet",
                "HIME": "Hime",
                "KAE": "Kae",
                "KAI": "Kai",
                "KRILA": "Krila",
                "KYOKO": "Kyoko",
                "KYOUSUKE": "Kyosuke",
                "MANAGER": "Store Manager",
                "MARC": "Marc",
                "MIXEDPOPPO": "Marie Poppo (Mixed)",
                "NANAKO": "Nanako",
                "PEAT": "Peat",
                "POPPO": "Marie Poppo",
                "QP": "QP",
                "QPD": "QP (Dangerous)",
                "ROBOT": "Robo Ball",
                "SAKI": "Saki",
                "SEAGULL": "Seagull / Jonathan",
                "SHAM": "Sham",
                "SHERRY": "Sherry",
                "SORA": "Sora",
                "SORAMILITARY": "Sora (Military)",
                "STARBREAKER": "Star Breaker",
                "SUGURI": "Suguri",
                "SUGV2": "Suguri (Ver.2)",
                "SWEETBREAKER": "Sweet Breaker",
                "SYURA": "Syura",
                "TOMOMO": "Tomomo",
                "YUKI": "Yuki",
                "DLC17A": "Mei",
                "DLC17B": "Natsumi",
				"DLC19A": "Nico",
				"DLC19B": "Arthur",
                "DLC20A": "Iru",
				"DLC20B": "Mira"
            };

            card_names = {
                "ACCELERATOR": "Accelerator",
                "ACCELHYPER": "Accel Hyper",
                "AIRSTRIKE": "Air Strike",
                "AMBRUSH": "Ambrush",
                "ANOTHERULTIMATE": "Another Ultimate Weapon",
                "ASSAULT": "Assault",
                "AVENGER": "Unforgiving Avenger",
                "BADPUDDING": "Bad Pudding",
                "BANNEDFORLIFE": "Banned for Life",
                "BEYONDHELL": "Beyond Hell",
                "BIGBANGBELL": "Big Bang Bell",
                "BIGMAGNUM": "Big Magnum",
                "BIGROCKET": "x16 Big Rocket",
                "BINDINGCHAINS": "Binding Chains",
                "BLAZING": "Blazing!",
                "BLUECROW": "Blue Crow the Second",
                "BRUTALPRANK": "Brutal Prank",
                "CASTOFF": "Cast Off",
                "CHARIOT": "Lonely Chariot",
                "CHARM": "Unlucky Charm",
                "CLOUDOFSEAGULLS": "Cloud of Seagulls",
                "COMPLETION": "Completion Reward",
                "CRYSTALBARRIER": "Crystal Barrier",
                "DANGEROUSPUDDING": "Dangerous Pudding",
                "DARKSIDE": "Dark Side of Business",
                "DASH": "Dash",
                "DELTAFIELD": "Delta Field",
                "DEPLOYBITS": "Deploy Bits",
                "DINNER": "Dinner",
                "EGG": "Golden Egg",
                "EVILSPYWORKS1": "Evil Spy Works: Preparation",
                "EVILSPYWORKS2": "Evil Spy Works: Execution",
                "EXCHANGE": "Exchange",
                "EXTEND": "Extend",
                "EXTENSION": "Extension",
                "FINALBATTLE": "Final Battle",
                "FINALSURGERY": "Final Surgery",
                "FLAMETHROWER": "Flamethrower",
                "FLIPOUT": "Flip Out",
                "FLYINGPIRATE": "Flying Pirate",
                "FORCEDREVIVAL": "Forced Revival",
                "GAMBLE": "Gamble",
                "GENTLEMAN": "Gentleman's Battle",
                "GIFTEXCHANGE": "Gift Exchange",
                "GOAWAY": "Go Away",
                "GUARDIAN": "Guardian of Blooming Flowers",
                "HAIRLOCK": "Witch's Hair Lock",
                "HEAT300": "Heat 300%",
                "HEREANDTHERE": "Here and There",
                "HOLYNIGHT": "Holy Night",
                "HYPERMODE": "Hyper Mode",
                "ICECREAM": "Miracle Red Bean Ice Cream",
                "IMMOVABLE": "Immovable Object",
                "INVASION": "Invasion",
                "JONATHAN": "Jonathan Rush",
                "LIFEGUARD": "Lifeguard on the White Beach",
                "LITTLEWAR": "Little War",
                "LONGDISTANCE": "Long Distance Shot",
                "LOSTCHILD": "Lost Child",
                "MAGICALMASSACRE": "Magical Massacre",
                "MAGICALREVENGE": "Magical Revenge",
                "MASTERMIND": "Evil Mastermind",
                "MIMIC": "Mimic",
                "MIMYUUSHAMMER": "Mimyuu's Hammer",
                "NICEJINGLE": "Nice Jingle",
                "NICEPRESENT": "Nice Present",
                "OHMYFRIEND": "Oh My Friend",
                "ONFIRE": "I'm on Fire!",
                "OUTOFAMMO": "Out of Ammo",
                "OVERSEER": "Overseer",
                "PARTY": "Party Time",
                "PHENOMENOM": "Mix Phenomenom",
                "PIGGYPANK": "Piggy Bank",
                "PIRATES": "Do Pirates Fly in the Sky?",
                "PIYOPIYOPROCESSION": "Piyopiyo Procession",
                "PLAYOFTHEGODS": "Play of the Gods",
                "PLUSHIEMASTER": "Plushie Master",
                "POWER": "Price of Power",
                "PRESENT": "Nice Present",
                "PRESENTTHIEF": "Present Thief",
                "PRESIDENT": "President's Privilege",
                "PRINCESS": "Princess' Privilege",
                "PROTAGONIST": "Protagonist's Privilege",
                "PUDDING": "Pudding",
                "QUICKRESTORATION": "Quick Restoration",
                "RAINBOW": "Rainbow-Colored Circle",
                "REFLECTIVE": "Reflective Shield",
                "RESEARCH": "Passionate Research",
                "REVERSE": "Reverse Attribute Field",
                "REVIVALOFSTARS": "Revival of Stars",
                "SAKISCOOKIE": "Saki's Cookie",
                "SANTASJOB": "Santa's Job",
                "SCRAMBLEDEVE": "Scrambled Eve",
                "SEALEDGUARDIAN": "Sealed Guardian",
                "SEALEDMEMORIES": "Sealed Memories",
                "SELFDESTRUCT": "Self-Destruct",
                "SHIELDCOUNTER": "Shield Counter",
                "SHIELD": "Shield",
                "SKYRESTAURANT": "Sky Restaurant 'Pures'",
                "SOLICITACION": "Scary Solicitacion",
                "SPECS": "Extraordinary Specs",
                "STARBLAZINGFUSE": "Star-Blazing Fuse",
                "STARBLAZINGLIGHT": "Star-Blazing Light",
                "STEALTH": "Stealth On!",
                "STIFFCRYSTAL": "Stiff Crystal",
                "SUPERALLOUTMODE": "Super All-Out Mode",
                "SWEETBATTLE": "Sweet Battle",
                "TACTICALRETREAT": "Tactical Retreat",
                "TOYSTORE": "For the Future of the Toy Store",
                "TRAGEDY": "Tragedy in the Dead of Night",
                "TREASURETHIEF": "Treasure Thief",
                "TURBOCHARGED": "Turbocharged",
                "UBIQUITOUS": "Ubiquitous",
                "USABIT": "Rbits",
                "WANTED": "Wanted",
                "WARUDAMACHINE": "Waruda Machine: Blast off!",
                "WEAPONINTHESUN": "Ultimate Weapon in the Sun",
                "WEAREWARUDA": "We are Waruda",
                "WINDY": "Windy Enchantement",
            };

            event_names = {
                "STAT_EVT_9G": "Brown Treasure Chest",
                "STAT_EVT_11B": "Big the Jonathan",
                "STAT_POPPO_POINTS": "Big Poppo"
            };

            event_stats_boss_names = ["STAT_EVT_11B", "STAT_POPPO_POINTS"];
            event_stats_collectible_names = ["STAT_EVT_9G"];

            chara_datatable = $('#dt_chara_stats').DataTable({
                "responsive": false,
                "paging": false,
                "searching": false,
                "info": false,
                "columnDefs": [
                    { "width": "5%", "type": 'alt-string', "targets": 0 },
                    { "type": "num-fmt", "targets": 3 }
                ],
                "order": [[ 1, "desc" ]]
            });
            card_datatable = $('#dt_card_stats').DataTable(datatable_definitions);
            event_datatable = $('#dt_event_stats').DataTable(datatable_definitions);
            misc_datatable = $('#dt_misc_stats').DataTable({
                "responsive": false,
                "paging": false,
                "searching": false,
                "info": false,
                "columnDefs": [
                    { "width": "15%", "targets": 0 }
                ],
            }); //I need a wider column

            $("#stat_container").css("display", "block");

            var content = JSON.parse('<?php if(isset($_GET['uid']) || isset($_SESSION['steamid'])) { include('utils/StatParser.php'); echo json_encode(getStats($stats['stats'])); } else { echo "[]";}; ?>');

            for (var key in content[0])
            {
                console.log()
                if(key.substr(0, 5) == "DLC20")
                {
                    chara_datatable.row.add(
                    [
                        "<img class='chara_icon' title=\""+((chara_names[key] != undefined) ? chara_names[key] : "Unknown/Unreleased Character ("+key+")" )+"\" alt=\""+((chara_names[key] != undefined) ? chara_names[key] : "Unknown/Unreleased Character ("+key+")" )+"\" src='imgs/chara/"+((chara_names[key] != undefined) ? key : "UNKNOWN" )+".png'>",
                        "<abbr title=\"Iru and Mira's played games stats are glitched, causing to have more played games that it should. To avoid confusion, this stat along with the winrate won't be displayed\">N/A</abbr>",
                        (content[0][key]['wins'] != undefined) ? content[0][key]['wins'] : 0,
                        "<abbr title=\"Iru and Mira's played games stats are glitched, causing to have more played games that it should. To avoid confusion, this stat along with the winrate won't be displayed\">N/A</abbr>"
                    ]).draw(false);
                }
                else
                {
                    chara_datatable.row.add(
                    [
                        "<img class='chara_icon' title=\""+((chara_names[key] != undefined) ? chara_names[key] : "Unknown/Unreleased Character ("+key+")" )+"\" alt=\""+((chara_names[key] != undefined) ? chara_names[key] : "Unknown/Unreleased Character ("+key+")" )+"\" src='imgs/chara/"+((chara_names[key] != undefined) ? key : "UNKNOWN" )+".png'>",
                        (content[0][key]['games'] != undefined) ? content[0][key]['games'] : 0,
                        (content[0][key]['wins'] != undefined) ? content[0][key]['wins'] : 0,
                        (content[0][key]['games'] >= 10) ? (Math.ceil(((content[0][key]['wins'] != undefined) ? content[0][key]['wins'] : 0)/((content[0][key]['games'] != undefined) ? content[0][key]['games'] : 0)*100)+"%") : ("<abbr title='Not enough games to display Win Rate'>N/A</abbr>")
                    ]).draw(false);
                }
            }

            for (var key in content[1])
            {
                card_datatable.row.add(
                [
                    "<img class='chara_icon' title=\""+((card_names[key] != undefined) ? card_names[key] : "Unknown/Unreleased Card ("+key+")" )+"\" alt=\""+((card_names[key] != undefined) ? card_names[key] : "Unknown/Unreleased Card ("+key+")" )+"\" src='imgs/card/"+key+".png'>",
                    content[1][key]
                ]).draw(false);
            }

            for (var key in content[2])
            {
                var value;
                if(event_stats_boss_names.includes(key))
                    value = "<abbr title='Damage Dealt'>"+content[2][key]+"</abbr>";
                else if(event_stats_collectible_names.includes(key))
                    value = "<abbr title='Items Collected'>"+content[2][key]+"</abbr>";
                event_datatable.row.add(
                [
                    "<img class='chara_icon' title=\""+((event_names[key] != undefined) ? event_names[key] : "Unknown Event Value ("+key+")" )+"\" alt=\""+((event_names[key] != undefined) ? event_names[key] : "Unknown Event Value ("+key+")" )+"\" src='imgs/event/"+((event_names[key] != undefined) ? key : "UNKNOWN" )+".png'>",
                    value
                ]).draw(false);
            }

            for (var key in content[3])
            {
                misc_datatable.row.add(
                [
                    "<code>"+key+"</code>",
                    content[3][key]
                ]).draw(false);
            }
        }
    }

    function changeTab(contentId, tab)
    {
        x = document.getElementsByClassName("tabcontent");
        for (var i = 0; i < x.length; i++)
            x[i].style.display = "none";

        if(!tab)
            x[contentId.value].style.display = "block";
        else
        {
            var x = document.getElementsByClassName("nav-link");
            for (var i = 0; i < x.length; i++)
                x[i].classList.remove("active");

            tab.classList.add("active");
            document.getElementById(contentId).style.display = "block";
        }

        chara_datatable.columns.adjust();
        card_datatable.columns.adjust();
        event_datatable.columns.adjust();
        misc_datatable.columns.adjust();
    }

    //animation fix
    function toggle_menu()
    {
        $("#menu-icon").toggleClass('flip');
        if(document.getElementById("mobile_menu").style.display == "none")
        {
            document.getElementById("mobile_menu").style.WebkitAnimation = "fadeIn 0.5s";
            document.getElementById("mobile_menu").style.animation = "fadeIn 0.5s";
            document.getElementById("mobile_menu").style.opacity = "1.0";
            document.getElementById("mobile_menu").style.display = "block"
        }
        else
        {
            document.getElementById("mobile_menu").style.WebkitAnimation = "fadeOut 0.5s";
            document.getElementById("mobile_menu").style.animation = "fadeOut 0.5s";
            document.getElementById("mobile_menu").style.opacity = "0.0";
            setTimeout(function(){document.getElementById("mobile_menu").style.display = "none"; }, 500);
        }
    }

</script>
