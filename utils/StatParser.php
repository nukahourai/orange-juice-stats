<?php
$chara_stats = [];
$card_stats = [];
$event_stats = [];
$misc_stats = [];

function getStats($stats)
{
    //event strings
    $ev_str = [ "STAT_EVT_11B", "STAT_POPPO_POINTS", "STAT_EVT_9G" ];
    $regex_chara = "/^STAT_(.+?(?=_))_ONLINE_(GAMES|WINS)$/";
    $regex_card = "/^STAT_CARD_(.+?(?=_))_CHOSEN$/";
    $regex_nath_tomato = "/^STAT_XMAS(\d)$/";
    $regex_miscstats = "/^(STAT_TEMP\d)$/";

    for($x=0;$x < sizeof($stats);$x++)
    {
        $groups;
        if(preg_match($regex_chara, $stats[$x]['name'], $groups))
        {
            if($groups[2] == 'GAMES')
                $chara_stats[$groups[1]]['games'] = $stats[$x]['value'];
            else
                $chara_stats[$groups[1]]['wins'] = $stats[$x]['value'];
        }
        elseif(preg_match($regex_card, $stats[$x]['name'], $groups))
            $card_stats[$groups[1]] = $stats[$x]['value'];
        elseif(preg_match($regex_nath_tomato, $stats[$x]['name'], $groups))
        {
            switch($groups[1])
            {
                case "1":
                    $chara_stats['DLC9A']['games'] = $stats[$x]['value'];
                    break;
                case "2":
                    $chara_stats['DLC9A']['wins'] = $stats[$x]['value'];
                    break;
                case "4":
                    $chara_stats['DLC9B']['games'] = $stats[$x]['value'];
                    break;
                case "5":
                    $chara_stats['DLC9B']['wins'] = $stats[$x]['value'];
                    break;
            }
        }
        elseif(preg_match($regex_miscstats, $stats[$x]['name'], $groups))
            $misc_stats[$groups[1]] = $stats[$x]['value'];
        elseif(in_array($stats[$x]['name'], $ev_str))
            $event_stats[$stats[$x]['name']] = $stats[$x]['value'];
        else
            continue;
    }

    return [$chara_stats, $card_stats, $event_stats, $misc_stats];
}

?>